package com.deutsche.dba.web;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import deutschebank.core.Controller;

@Path("/individual")
public class IndividualPort {
	final   Controller controller = new Controller();

    @GET
    @Path("/instrument")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllInstrument()
    {
    	String theInstruments = controller.getAllInstrument();
        return Response.ok(theInstruments, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/counterparty")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCounterparty()
    {
    	String cps = controller.getAllCounterparty();
        return Response.ok(cps, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/deal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDeal()
    {
    	String theDeals = controller.getAllDeal();
        return Response.ok(theDeals, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    

}
