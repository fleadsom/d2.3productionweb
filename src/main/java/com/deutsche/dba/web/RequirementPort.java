package com.deutsche.dba.web;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import deutschebank.core.Controller;

@Path("/graph")
public class RequirementPort {
	final   Controller controller = new Controller();

	@GET
    @Path("/gone")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGOne()
    {
    	String theGraphOnes = controller.getGraphOne();
        return Response.ok(theGraphOnes, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @GET
    @Path("/gtwo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGTwo()
    {
    	String cps = controller.getGraphTwo();
        return Response.ok(cps, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @POST
    @Path("/gthree")
    public Response getGThree(@FormParam("graphThreeCounterP") String counterPartyName)
    {
    	String theGraphThrees = controller.getGraphThree(counterPartyName);
        return Response.ok(theGraphThrees, MediaType.APPLICATION_JSON_TYPE).build();
    }

    
    

}
