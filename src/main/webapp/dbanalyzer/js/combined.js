var rootURL = "rws/graph";
//var loginStatus = false;
var mixedGraphShow = false;
var buyGraphShow = false;
var sellGraphShow = false;
$(document).ready(function() {
	$("#mixedBtn").click(function(){
		getFromBE();
		mixedGraphShow = true;
		buyGraphShow = false;
		sellGraphShow = false;
    });
	$("#buyBtn").click(function(){
		getFromBEBuy();
		mixedGraphShow = false;
		buyGraphShow = true;
		sellGraphShow = false;
    });
	$("#sellBtn").click(function(){
		getFromBESell();
		mixedGraphShow = false;
		buyGraphShow = false;
		sellGraphShow = true;
    });
});

function getFromBE(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/gone',
		dataType: "json", // data type of response
                
		success: function (result) {
			convertInstrumentsJSON(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all mixed graph " );
    });
}

function getFromBEBuy(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/gone',
		dataType: "json", // data type of response
                
		success: function (result) {
			convertInstrumentsJSONBuy(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all buy graph " );
    });
}

function getFromBESell(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/gone',
		dataType: "json", // data type of response
                
		success: function (result) {
			convertInstrumentsJSONSale(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all sell graph " );
    });
}

function convertInstrumentsJSON(data) {
	var result = {
		"name" : "Instrument",
		"children" : []
	};

	var instrName = data[0]["instrumentName"];
	var personName = data[0]["counterPartyName"];
	var tmpInstr = {
		"name" : instrName,
		"children" : []
	};
	var tmpPerson = {
		"name" : personName,
		"children" : []
	};
	result.children.push(tmpInstr);
	result.children[0].children.push(tmpPerson);

	for (i = 0; i < data.length; i++) {
		// document.getElementById("demo").innerHTML += i + "<br>";
		if (instrName === data[i]["instrumentName"]) {
			// document.getElementById("demo").innerHTML +=
			// data[i]["instrumentName"] + "<br>";

			if (personName === data[i]["counterPartyName"]) {
				var len = result.children.length - 1;
				var type = "";
				if (data[i].dealType == "S") {
					type = "Sale";
				} else {
					type = "Buy";
				}
				var tmp = {
					"name" : type,
					"size" : data[i].dealAmount * data[i].dealQuantity
				};
				result["children"][len]["children"][result.children[len]["children"].length - 1]["children"]
						.push(tmp);
			} else {
				personName = data[i]["counterPartyName"];
				var len = result.children.length - 1;
				var tmpPrsn = {
					"name" : data[i]["counterPartyName"],
					"children" : []
				};
				var type = "";
				if (data[i].dealType == "S") {
					type = "Sale";
				} else {
					type = "Buy";
				}
				var tmp = {
					"name" : type,
					"size" : data[i].dealAmount * data[i].dealQuantity
				};
				tmpPrsn["children"].push(tmp);
				result["children"][len]["children"].push(tmpPrsn);
			}
		} else {
			instrName = data[i]["instrumentName"];
			personName = data[i]["counterPartyName"];
			var type = "";
			if (data[i].dealType == "S") {
				type = "Sale";
			} else {
				type = "Buy";
			}
			var tmp = {
				"name" : type,
				"size" : data[i].dealAmount * data[i].dealQuantity
			};

			var tmpPrsn = {
				"name" : data[i]["counterPartyName"],
				"children" : [ tmp, ]
			};

			var tmpIn = {
				"name" : data[i]["instrumentName"],
				"children" : [ tmpPrsn, ]
			};
			result["children"].push(tmpIn);
		}

	}
	getMixedGraph(result);
}

function convertInstrumentsJSONSale(data)
{
  var result = {"name" : "Instrument", "children": []};
  var instrName = data[0]["instrumentName"];
  var personName = data[0]["counterPartyName"];
  var tmpInstr = {"name" : instrName, "children": []};
  var tmpPerson = {"name" : personName, "children": []};
  result.children.push(tmpInstr);
  result.children[0].children.push(tmpPerson);


  for (i = 0; i < data.length; i++) {
    if (instrName === data[i]["instrumentName"]) {

      if (personName === data[i]["counterPartyName"]) {
        var len = result.children.length-1;
        var type = "";
        if (data[i].dealType == "S") {
          type = "Sale";
        } else {
          //type = "Buy";
          continue;
        }
        var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][len]["children"][result.children[len]["children"].length-1]["children"].push(tmp);
      } else {
        personName = data[i]["counterPartyName"];
        var len = result.children.length-1;
        var tmpPrsn = {"name" : data[i]["counterPartyName"], "children": []};
        var type = "";
        if (data[i].dealType == "S") {
          type = "Sale";
        } else {
          //type = "Buy";
          continue;
        }
        var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};
        tmpPrsn["children"].push(tmp);
        result["children"][len]["children"].push(tmpPrsn);
      }
    } else {
      instrName = data[i]["instrumentName"];
      personName = data[i]["counterPartyName"];
      var type = "";
      if (data[i].dealType == "S") {
        type = "Sale";
      } else {
        //type = "Buy";
        continue;
      }
      var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};

      var tmpPrsn = {"name" : data[i]["counterPartyName"], "children": [tmp,]};

      var tmpIn = {"name" : data[i]["instrumentName"], "children": [tmpPrsn,]};
      result["children"].push(tmpIn);
    }

  }
  console.log(JSON.stringify(result));
  getSellGraph(result);

}

function convertInstrumentsJSONBuy(data)
{
  var result = {"name" : "Instrument", "children": []};
  var instrName = data[0]["instrumentName"];
  var personName = data[0]["counterPartyName"];
  var tmpInstr = {"name" : instrName, "children": []};
  var tmpPerson = {"name" : personName, "children": []};
  result.children.push(tmpInstr);
  result.children[0].children.push(tmpPerson);


  for (i = 0; i < data.length; i++) {
    if (instrName === data[i]["instrumentName"]) {

      if (personName === data[i]["counterPartyName"]) {
        var len = result.children.length-1;
        var type = "";
        if (data[i].dealType == "S") {
          //type = "Sale";
          continue;
        } else {
          type = "Buy";
          
        }
        var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][len]["children"][result.children[len]["children"].length-1]["children"].push(tmp);
      } else {
        personName = data[i]["counterPartyName"];
        var len = result.children.length-1;
        var tmpPrsn = {"name" : data[i]["counterPartyName"], "children": []};
        var type = "";
        if (data[i].dealType == "S") {
          //type = "Sale";
          continue;
        } else {
          type = "Buy";
          
        }
        var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};
        tmpPrsn["children"].push(tmp);
        result["children"][len]["children"].push(tmpPrsn);
      }
    } else {
      instrName = data[i]["instrumentName"];
      personName = data[i]["counterPartyName"];
      var type = "";
      if (data[i].dealType == "S") {
        //type = "Sale";
        continue;
      } else {
        type = "Buy";
        
      }
      var tmp = {"name": type, "size": data[i].dealAmount*data[i].dealQuantity};

      var tmpPrsn = {"name" : data[i]["counterPartyName"], "children": [tmp,]};

      var tmpIn = {"name" : data[i]["instrumentName"], "children": [tmpPrsn,]};
      result["children"].push(tmpIn);
    }

  }
  console.log(JSON.stringify(result));
  getBuyGraph(result);

}

function getMixedGraph(data){
	var svg = d3.select("div#mixedGraphArea svg"),
    margin = 20,
    diameter = +svg.attr("width"),
    g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

	var pack = d3.pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

	
	var root = d3.hierarchy(data);
	root
	.sum(function(d) { return d.size; })
    .sort(function(a, b) { return b.value - a.value; });
  
  var focus = root,
      nodes = pack(root).descendants(),
      view;
  
  var circle = g.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
      .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
 	  .attr("fill", "rgb(31, 119, 180)")
	  .attr("fill-opacity", ".25")
	  .attr("stroke", "rgb(31, 119, 180)")
	  .attr("stroke-width", "1px")
	  .style("fill", function(d) { return !d.children ? d.data.name == "Buy" ? "orange" : "rgb(0,0,255)" : null; })     
      .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });
  
  var text = g.selectAll("text")
    .data(nodes)
    .enter().append("text")
      .attr("class", "label")
      .style("fill-opacity", function(d) { return d.parent === root ? 1 : 0; })
      .text(function(d) { return d.parent ? d.data.name : null;})
      .text(function(d) { return !d.children ? (d.data.name + ":" + d.data.size) : d.data.name; })
      .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});
      
  
  var node = g.selectAll("circle,text");
  svg
      .on("click", function() { zoom(root); });
  zoomTo([root.x, root.y, root.r * 2 + margin]);
  
  function zoom(d) {
    var focus0 = focus; focus = d;
    var transition = d3.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .tween("zoom", function(d) {
          var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
          return function(t) { zoomTo(i(t)); };
        });
  
    transition.selectAll("text")
      .filter(function(d) { return d.parent === focus || this.style.display === "inline"; })
        .style("fill-opacity", function(d) { return d.parent === focus ? 1 : 0; })
        .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
        .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; })
        .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});

  }
  
  function zoomTo(v) {
    var k = diameter / v[2]; view = v;
    node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
    circle.attr("r", function(d) { return d.r * k; });
  }

  if (mixedGraphShow){
		$("div#mixedGraphArea").show();
		$("div#buyGraphArea").hide();
		$("div#sellGraphArea").hide();
	} else {
		$("div#mixedGraphArea").hide();
	}
	
}

function getBuyGraph(data){
	
	var svg = d3.select("div#buyGraphArea svg"),
    margin = 20,
    diameter = +svg.attr("width"),
    g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

	var pack = d3.pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

	
	var root = d3.hierarchy(data);
	root
	.sum(function(d) { return d.size; })
    .sort(function(a, b) { return b.value - a.value; });
  
  var focus = root,
      nodes = pack(root).descendants(),
      view;
  
  var circle = g.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
      .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
 	  .attr("fill", "rgb(31, 119, 180)")
	  .attr("fill-opacity", ".25")
	  .attr("stroke", "rgb(31, 119, 180)")
	  .attr("stroke-width", "1px")
	  .style("fill", function(d) { return !d.children ? d.data.name == "Buy" ? "orange" : "rgb(0,0,255)" : null; })     
      .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });
  
  var text = g.selectAll("text")
    .data(nodes)
    .enter().append("text")
      .attr("class", "label")
      .style("fill-opacity", function(d) { return d.parent === root ? 1 : 0; })
      .text(function(d) { return d.parent ? d.data.name : null;})
      .text(function(d) { return !d.children ? (d.data.name + ":" + d.data.size) : d.data.name; })
      .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});
      
  
  var node = g.selectAll("circle,text");
  svg
      .on("click", function() { zoom(root); });
  zoomTo([root.x, root.y, root.r * 2 + margin]);
  
  function zoom(d) {
    var focus0 = focus; focus = d;
    var transition = d3.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .tween("zoom", function(d) {
          var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
          return function(t) { zoomTo(i(t)); };
        });
  
    transition.selectAll("text")
      .filter(function(d) { return d.parent === focus || this.style.display === "inline"; })
        .style("fill-opacity", function(d) { return d.parent === focus ? 1 : 0; })
        .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
        .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; })
        .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});

  }
  
  function zoomTo(v) {
    var k = diameter / v[2]; view = v;
    node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
    circle.attr("r", function(d) { return d.r * k; });
  }

  if (buyGraphShow){
		$("div#mixedGraphArea").hide();
		$("div#buyGraphArea").show();
		$("div#sellGraphArea").hide();
	} else {
		$("div#buyGraphArea").hide();
	}

}


function getSellGraph(data){
	
	var svg = d3.select("div#sellGraphArea svg"),
    margin = 20,
    diameter = +svg.attr("width"),
    g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

	var pack = d3.pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

	
	var root = d3.hierarchy(data);
	root
	.sum(function(d) { return d.size; })
    .sort(function(a, b) { return b.value - a.value; });
  
  var focus = root,
      nodes = pack(root).descendants(),
      view;
  
  var circle = g.selectAll("circle")
    .data(nodes)
    .enter().append("circle")
      .attr("class", function(d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
 	  .attr("fill", "rgb(31, 119, 180)")
	  .attr("fill-opacity", ".25")
	  .attr("stroke", "rgb(31, 119, 180)")
	  .attr("stroke-width", "1px")
	  .style("fill", function(d) { return !d.children ? d.data.name == "Buy" ? "orange" : "rgb(0,0,255)" : null; })     
      .on("click", function(d) { if (focus !== d) zoom(d), d3.event.stopPropagation(); });
  
  var text = g.selectAll("text")
    .data(nodes)
    .enter().append("text")
      .attr("class", "label")
      .style("fill-opacity", function(d) { return d.parent === root ? 1 : 0; })
      .text(function(d) { return d.parent ? d.data.name : null;})
      .text(function(d) { return !d.children ? (d.data.name + ":" + d.data.size) : d.data.name; })
      .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});
      
  
  var node = g.selectAll("circle,text");
  svg
      .on("click", function() { zoom(root); });
  zoomTo([root.x, root.y, root.r * 2 + margin]);
  
  function zoom(d) {
    var focus0 = focus; focus = d;
    var transition = d3.transition()
        .duration(d3.event.altKey ? 7500 : 750)
        .tween("zoom", function(d) {
          var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
          return function(t) { zoomTo(i(t)); };
        });
  
    transition.selectAll("text")
      .filter(function(d) { return d.parent === focus || this.style.display === "inline"; })
        .style("fill-opacity", function(d) { return d.parent === focus ? 1 : 0; })
        .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
        .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; })
        .attr("font-size", function(d) { return !d.children ? "11px" : "34px";});

  }
  
  function zoomTo(v) {
    var k = diameter / v[2]; view = v;
    node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
    circle.attr("r", function(d) { return d.r * k; });
  }

  if (sellGraphShow){
		$("div#mixedGraphArea").hide();
		$("div#buyGraphArea").hide();
		$("div#sellGraphArea").show();
	} else {
		$("div#sellGraphArea").hide();
	}

}

