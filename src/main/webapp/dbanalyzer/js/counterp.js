var rootURL = "rws/graph";
//var loginStatus = false;

$(document).ready(function() {
	$( "#graphThreeForm" ).submit(function( event ) {
		getGraphThreeData()
		event.preventDefault();
	});
	
});

function getGraphThreeData(){
	formdata = $('form');
    fds = formdata.serialize();
	var request = $.ajax({
		method: 'POST',
		url: rootURL + '/gthree',
		dataType: "json", // data type of response
		data: fds,        
		success: function (result) {
			convertNameJSON(result);
			
			$( 'form' ).trigger("reset");
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all  graph three" );
    });
}

function convertNameJSON(data)
{
  var result = {"name" : data[0]["counterPartyName"], "children": []};
 
  var sale = {"name": "Sale", "children": []};
  result.children.push(sale);

  var buy = {"name": "Buy", "children": []};
  result.children.push(buy);

  /*var ind = (data[0]["dealType"] == "S") ? 0 : 1;
  var tmp = {"name": data[0]["instrumentName"], "children": []};
  result["children"][ind]["children"].push(tmp);
*/

  //document.getElementById("demo").innerHTML = JSON.stringify(result) + "<br>";

  var instrName = "";

  for (i = 0; i < data.length; i++) {
    if (data[i]["dealType"] == "S") {
      /*if (result["children"][0]["children"].length == 0) {
        var tmp = {"name": data[i]["instrumentName"], "children": []};
        result["children"][0]["children"].push(tmp);
      }*/

     // document.getElementById("demo").innerHTML += JSON.stringify(result) + "<br>";

      if (instrName == data[i]["instrumentName"]) {
        if (result["children"][0]["children"].length == 0) {
          var tmp = {"name": data[i]["instrumentName"], "children": []};
          result["children"][0]["children"].push(tmp);
        }

        var len = result["children"][0]["children"].length-1;
        var tmpObj = {"name": data[i]["dealID"], "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][0]["children"][len]["children"].push(tmpObj);
      } else {
        instrName = data[i]["instrumentName"];
        var tmp = {"name": instrName, "children": []};
        result["children"][0]["children"].push(tmp);

        var len = result["children"][0]["children"].length-1;
        var tmpObj = {"name": data[i]["dealID"], "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][0]["children"][len]["children"].push(tmpObj);
      }
    } else {   
      /*if (result["children"][1]["children"].length == 0) {
        var tmp = {"name": data[i]["instrumentName"], "children": []};
        result["children"][1]["children"].push(tmp);
      }*/

      //document.getElementById("demo").innerHTML += JSON.stringify(result) + "<br>";


      if (instrName == data[i]["instrumentName"]) {
        if (result["children"][0]["children"].length == 0) {
          var tmp = {"name": data[i]["instrumentName"], "children": []};
          result["children"][0]["children"].push(tmp);
        }

        var len = result["children"][1]["children"].length-1;
        var tmpObj = {"name": data[i]["dealID"], "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][1]["children"][len]["children"].push(tmpObj);
      } else {
        instrName = data[i]["instrumentName"];
        var tmp = {"name": instrName, "children": []};
        result["children"][1]["children"].push(tmp);

        var len = result["children"][1]["children"].length-1;
        var tmpObj = {"name": data[i]["dealID"], "size": data[i].dealAmount*data[i].dealQuantity};
        result["children"][1]["children"][len]["children"].push(tmpObj);
      }

    }

  }

  //document.getElementById("demo").innerHTML += JSON.stringify(result) + "<br>";
console.log(JSON.stringify(result));
  showGraphThree(result);
}

function showGraphThree(data){
	 const width = window.innerWidth,
     height = window.innerHeight,
     maxRadius = (Math.min(width, height) / 2) - 5;

 const formatNumber = d3.format(',d');

 const x = d3.scaleLinear()
     .range([0, 2 * Math.PI])
     .clamp(true);

 const y = d3.scaleSqrt()
     .range([maxRadius*.1, maxRadius]);

 const partition = d3.partition();

 const arc = d3.arc()
     .startAngle(d => x(d.x0))
     .endAngle(d => x(d.x1))
     .innerRadius(d => Math.max(0, y(d.y0)))
     .outerRadius(d => Math.max(0, y(d.y1)));

 const middleArcLine = d => {
     const halfPi = Math.PI/2;
     const angles = [x(d.x0) - halfPi, x(d.x1) - halfPi];
     const r = Math.max(0, (y(d.y0) + y(d.y1)) / 2);

     const middleAngle = (angles[1] + angles[0]) / 2;
     const invertDirection = middleAngle > 0 && middleAngle < Math.PI; // On lower quadrants write text ccw
     if (invertDirection) { angles.reverse(); }

     const path = d3.path();
     path.arc(0, 0, r, angles[0], angles[1], invertDirection);
     return path.toString();
 };

 const textFits = d => {
     const CHAR_SPACE = 6;

     const deltaAngle = x(d.x1) - x(d.x0);
     const r = Math.max(0, (y(d.y0) + y(d.y1)) / 2);
     const perimeter = r * deltaAngle;

     return d.data.name.length * CHAR_SPACE < perimeter;
 };

 const svg = d3.select('div#graphThree').append('svg')
     .style('width', '100vw')
     .style('height', '100vh')
     .attr('viewBox', `${-width / 2} ${-height / 2} ${width} ${height}`)
     .on('click', () => focusOn()); // Reset zoom on canvas click

    var root = d3.hierarchy(data);
     root.sum(d => d.size);

     const slice = svg.selectAll('g.slice')
         .data(partition(root).descendants());

     slice.exit().remove();

     const newSlice = slice.enter()
         .append('g').attr('class', 'slice')
         .on('click', d => {
             d3.event.stopPropagation();
             focusOn(d);
         });

     newSlice.append('title')
         .text(d => d.data.name + '\n' + formatNumber(d.value));
     
     newSlice.append('path')
         .attr('class', 'main-arc')
         .style('fill', function(d) { return d.parent ? d.data.name == "Buy" ? "orange" : d.data.name == 
         	"Sale" ? "rgb(0,0,255)" : (d.parent.data.name == "Buy" || d.parent.parent.data.name == "Buy") ? "#ffbb78" : (d.parent.data.name == "Sale" || d.parent.parent.data.name == "Sale") ? "#aec7e8" : null : "#b3b6b7"; })
         .attr('d', arc);

     newSlice.append('path')
         .attr('class', 'hidden-arc')
         .attr('id', (_, i) => `hiddenArc${i}`)
         .attr('d', middleArcLine);

     const text = newSlice.append('text')
         .attr('display', d => textFits(d) ? null : 'none');

     // Add white contour
     text.append('textPath')
         .attr('startOffset','50%')
         .attr('xlink:href', (_, i) => `#hiddenArc${i}` )
         .text(d => d.data.name)
         .style('fill', 'none')
         .style('stroke', '#fff')
         .style('stroke-width', 5)
         .style('stroke-linejoin', 'round');

     text.append('textPath')
         .attr('startOffset','50%')
         .attr('xlink:href', (_, i) => `#hiddenArc${i}` )
         .text(d => d.data.name);
 

 function focusOn(d = { x0: 0, x1: 1, y0: 0, y1: 1 }) {
     // Reset to top-level if no data point specified

     const transition = svg.transition()
         .duration(750)
         .tween('scale', () => {
             const xd = d3.interpolate(x.domain(), [d.x0, d.x1]),
                 yd = d3.interpolate(y.domain(), [d.y0, 1]);
             return t => { x.domain(xd(t)); y.domain(yd(t)); };
         });

     transition.selectAll('path.main-arc')
         .attrTween('d', d => () => arc(d));

     transition.selectAll('path.hidden-arc')
         .attrTween('d', d => () => middleArcLine(d));

     transition.selectAll('text')
         .attrTween('display', d => () => textFits(d) ? null : 'none');

     moveStackToFront(d);

     //

     function moveStackToFront(elD) {
         svg.selectAll('.slice').filter(d => d === elD)
             .each(function(d) {
                 this.parentNode.appendChild(this);
                 if (d.parent) { moveStackToFront(d.parent); }
             })
     }
 }
}