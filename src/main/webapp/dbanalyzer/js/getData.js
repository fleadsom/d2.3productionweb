var rootURL = "rws/individual";
// var loginStatus = false;

var dealTableShow = false;
var counterpartyTableShow = false;
var instrumentTableShow = false;

$(document).ready(function() {
	$("#dealBtn").click(function(){
		getDealGraph();
		dealTableShow = true;
		counterpartyTableShow = false;
		instrumentTableShow = false;
    });
	$("#counterpartyBtn").click(function(){
		getCounterpartyGraph();
		dealTableShow = false;
		counterpartyTableShow = true;
		instrumentTableShow = false;
    });
	$("#instrumentBtn").click(function(){
		getInstrumentGraph();
		dealTableShow = false;
		counterpartyTableShow = false;
		instrumentTableShow = true;
    });

});

function getDealGraph(){
	    var request = $.ajax({
			method: 'GET',
			url: rootURL + '/deal',
			dataType: "json", // data type of response
	                
			success: function (result) {
	                    displayAllDeal(result);
			}
		});
	    request.fail(function( jqXHR, textStatus, errorThrown )
	    {
	        alert( "Request " + textStatus + ", fail to get all deal " );
	    });
}

function getCounterpartyGraph(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/counterparty',
		dataType: "json", // data type of response
                
		success: function (result) {
			displayAllCounterparty(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all counterparty " );
    });
}

function getInstrumentGraph(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/instrument',
		dataType: "json", // data type of response
                
		success: function (result) {
			displayAllInstrument(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get all instrument " );
    });
}

function displayAllDeal(dealResult){
	var g = $("#rawDealTableArea");
	g.append("<p>Deal Table</p>");
	var col = [];
        for (var i = 0; i < dealResult.length; i++) {
            for (var key in dealResult[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < dealResult.length; i++) {

            tr = table.insertRow(-1);
            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = dealResult[i][col[j]];
            }
        }

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("rawDealTableArea");
        divContainer.innerHTML = "";
		divContainer.appendChild(table);
		
		 if (dealTableShow){
				$("div#rawDealTableArea").show();
				$("div#rawInstrumentTableArea").hide();
				$("div#rawCounterpartyTableArea").hide();
			} else {
				$("div#rawDealTableArea").hide();
			}

}

function displayAllCounterparty(dealResult){
	var g = $("#rawCounterpartyTableArea");

	var col = [];
	for (var i = 0; i < dealResult.length; i++) {
		for (var key in dealResult[i]) {
			if (col.indexOf(key) === -1) {
				col.push(key);
			}
		}
	}
	
	// CREATE DYNAMIC TABLE.
	var table = document.createElement("table");

	// CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
	var tr = table.insertRow(-1);                   // TABLE ROW.

	for (var i = 0; i < col.length; i++) {
		var th = document.createElement("th");      // TABLE HEADER.
		th.innerHTML = col[i];
		tr.appendChild(th);
	}

	// ADD JSON DATA TO THE TABLE AS ROWS.
	for (var i = 0; i < dealResult.length; i++) {
		tr = table.insertRow(-1);
		for (var j = 0; j < col.length; j++) {
			var tabCell = tr.insertCell(-1);
			tabCell.innerHTML = dealResult[i][col[j]];
		}
	}

	// FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
	var divContainer = document.getElementById("rawCounterpartyTableArea");
	divContainer.innerHTML = "";
	divContainer.appendChild(table);
	
	if (counterpartyTableShow){
		$("div#rawDealTableArea").hide();
		$("div#rawInstrumentTableArea").hide();
		$("div#rawCounterpartyTableArea").show();
	} else {
		$("div#rawCounterpartyTableArea").hide();
	}
}

function displayAllInstrument(dealResult){
	var g = $("#rawInstrumentTableArea");

	var col = [];
	for (var i = 0; i < dealResult.length; i++) {
		for (var key in dealResult[i]) {
			if (col.indexOf(key) === -1) {
				col.push(key);
			}
		}
	}
	
	// CREATE DYNAMIC TABLE.
	var table = document.createElement("table");

	// CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
	var tr = table.insertRow(-1);                   // TABLE ROW.

	for (var i = 0; i < col.length; i++) {
		var th = document.createElement("th");      // TABLE HEADER.
		th.innerHTML = col[i];
		tr.appendChild(th);
	}

	// ADD JSON DATA TO THE TABLE AS ROWS.
	for (var i = 0; i < dealResult.length; i++) {
		tr = table.insertRow(-1);
		for (var j = 0; j < col.length; j++) {
			var tabCell = tr.insertCell(-1);
			tabCell.innerHTML = dealResult[i][col[j]];
		}
	}

	// FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
	var divContainer = document.getElementById("rawInstrumentTableArea");
	divContainer.innerHTML = "";
	divContainer.appendChild(table);
	
	if (instrumentTableShow){
		$("div#rawDealTableArea").hide();
		$("div#rawInstrumentTableArea").show();
		$("div#rawCounterpartyTableArea").hide();
	} else {
		$("div#rawInstrumentTableArea").hide();
	}
}
