var rootURL = "rws/requirement";
var loginStatus = false;

$(document).ready(function() {
	$("#dealBtn").click(function(){
		getDealGraph();
    });
});

function getReqOne(){
	    var request = $.ajax({
			method: 'POST',
			url: rootURL + '/reqone',
			dataType: "json", // data type of response
			data: fds,
			
			success: function (result) {
	                    displayAllReqOne(result);
			}
		});
	    request.fail(function( jqXHR, textStatus, errorThrown )
	    {
	        alert( "Request " + textStatus + ", fail to get requirement one " );
	    });
}

function getReqTwo(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/reqtwo',
		dataType: "json", // data type of response
                
		success: function (result) {
			displayAllReqTwo(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get requirement two " );
    });
}

function getReqThree(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/reqthree',
		dataType: "json", // data type of response
                
		success: function (result) {
			displayAllReqThree(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get requirement three " );
    });
}

function getReqFour(){
	var request = $.ajax({
		method: 'GET',
		url: rootURL + '/reqfour',
		dataType: "json", // data type of response
                
		success: function (result) {
			displayAllReqFour(result);
		}
	});
    request.fail(function( jqXHR, textStatus, errorThrown )
    {
        alert( "Request " + textStatus + ", fail to get requirement four " );
    });
}

