<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper"
	class="deutschebank.core.ApplicationScopeHelper" scope="application" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="dbanalyzer/css/homeJSP.css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="dbanalyzer/js/main.js"></script>
<script src="dbanalyzer/js/userdetails_validator.js"></script>
<title>Welcome to Milash App</title>
<link rel="icon" type="image/png" href="https://www.db.com/company/img/favicon.ico" />
</head>

<body background="dbanalyzer/images/background.jpg">
<% String dbStatus = "DB NOT CONNECTED"; globalHelper.setInfo("Set any value here for application level access"); 
			boolean connectionStatus =	globalHelper.bootstrapDBConnection(); 
			if (connectionStatus) { dbStatus = "Your credentials are correct. Login Successful!"; } %>
<nav class="navbar navbar-default" style="background-color: #000000">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Milash</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="home.jsp">Home</a></li>
				<li class="dropdown" style="color: ~ffff"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">Visualization <span
						class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<li><a href="main.jsp">Stock price</a></li>
						<li><a href="combined.jsp">My counterparties</a></li>
						<li><a href="counterp.jsp">My deals</a></li>
					</ul></li>
				<li><a href="raw.jsp">Raw Data</a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><span
						class="glyphicon glyphicon-log-out"></span>Logout</a></li>
			</ul>
		</div>
	</nav>
	<div align="right">
		<div class="container">
			<img alt="logo"
				src="https://www.netzwerk-ebd.de/wp-content/uploads/2017/05/logo_square_dbblue-smaller.jpg"
				 width="30" align="right" hspace="15"> <span>Powered
				by</span>
		</div>
	</div>

	<div class="container-fluid">
		<div align="center">
			<h1>
				<a href="home.jsp">Welcome to Milash : Data Visualization App</a>
			</h1>
			<p style="color:blue;font-size:20px;"><%=dbStatus%></p>
			<img src="./dbanalyzer/images/cats.jpg" alt="home image" width="600"
				height="300">
		</div>


		<div class="boxed" align="left"
			style="color: black; font-family: verdana;">Milash is a Russian
			name that could be translated as “very cute”. We chose this name for
			the App, because we believe that data visualization should be “very
			cute”.</div>

		<div class="boxed" align="left"
			style="color: black; font-family: verdana; font-weight: bold;">
			Our Purpose <br>
		</div>

		<div class="boxed" align="left"
			style="color: black; font-family: verdana;">
			Bringing information easily to your screen. <br>
		</div>

		<div class="boxed2" align="left"
			style="color: black; font-family: verdana; font-weight: bold;">
			Our Mission <br>
		</div>

		<div class="boxed" align="left"
			style="color: black; font-family: verdana;">
			Translate complicated data into easy, intuitive understanding graphs
			and visualizations, but keep the raw data within reach for the
			customer. <br>
		</div>

		<div class="boxed2" align="left"
			style="color: black; font-family: verdana; font-weight: bold;">
			Our Vision <br>
		</div>

		<div class="boxed" align="left"
			style="color: black; font-family: verdana;">
			To be the preferred used app to dealers. <br>
		</div>

		<div class="boxed2" align="left"
			style="color: black; font-family: verdana; font-weight: bold;">
			Our Values <br>
		</div>

		<div class="boxed2" align="left"
			style="color: black; font-family: verdana;">

			<p>Example of unordered lists:</p>
			<ul class="a">
				<li>Integrity: doing what is right</li>
				<li>Sustainable Performance: have a performing app and
					incrementing functionalities</li>
				<li>Client Centricity: user-friendly displays</li>
				<li>Innovation: fancy graphs</li>
				<li>Discipline: low resources high performance</li>
				<li>Partnership: diverse team</li>
			</ul>

			<div class="boxed2" align="left"
				style="color: black; font-family: verdana; font-weight: bold;">
				In case of any suggestions for the future please contact:</div>

			<div class="boxed2" align="left"
				style="color: black; font-family: verdana; font-weight: bold;">
				milash.suggest@db.com <br>
			</div>
		</div>
	</div>
</body>
</html>
