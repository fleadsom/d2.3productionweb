<%-- Document : index Created on : 17-Jun-2016, 16:53:18 Author : Selvyn
--%> <%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper"
	class="deutschebank.core.ApplicationScopeHelper" scope="application" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
</script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous">
	</script>

<title>Welcome to Milash App</title>
<link rel="icon" type="image/png" href="https://www.db.com/company/img/favicon.ico" />
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/animate/animate.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/css-hamburgers/hamburgers.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/animsition/css/animsition.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/vendor/daterangepicker/daterangepicker.css">

<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/css/util.css">
<link rel="stylesheet" type="text/css" href="dbanalyzer/loginPage/css/main.css">

<script src="dbanalyzer/loginPage/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="dbanalyzer/loginPage/vendor/animsition/js/animsition.min.js"></script>
<script src="dbanalyzer/loginPage/vendor/bootstrap/js/popper.js"></script>
<script src="dbanalyzer/loginPage/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="dbanalyzer/loginPage/vendor/select2/select2.min.js"></script>
<script src="dbanalyzer/loginPage/vendor/daterangepicker/moment.min.js"></script>
<script src="dbanalyzer/loginPage/vendor/daterangepicker/daterangepicker.js"></script>
<script src="dbanalyzer/loginPage/vendor/countdowntime/countdowntime.js"></script>

<script src="dbanalyzer/js/main.js"></script>
<script src="dbanalyzer/js/userdetails_validator.js"></script>
</head>
<body>
	<% String dbStatus = "DB NOT CONNECTED"; globalHelper.setInfo("Set any value here for application level access"); 
			boolean connectionStatus =	globalHelper.bootstrapDBConnection(); 
			if (connectionStatus) { dbStatus = "You have successfully connected the Database server."; } %>
	<div class="limiter">
		<div class="container-login100"
			style="background-image: url('dbanalyzer/loginPage/images/deutsche-bank.jpg');">
			<div class="wrap-login100 p-t-20 p-b-40">
				<span class="login100-form-title p-b-41">
						<p><%=dbStatus%></p>
				</span> <% if (connectionStatus) { %>
				<form name="loginForm" action="home.jsp" id="loginForm"
					onsubmit="return validateForm()" method="post"
					class="login100-form validate-form p-b-33 p-t-5">

					<div class="wrap-input100 validate-input"
						data-validate="Enter username">
						<input class="input100" id="f_userid" type="text" name="usr"
							placeholder="Username" value="" required> <span
							class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>

					<div class="wrap-input100 validate-input"
						data-validate="Enter password">
						<input class="input100" type="password" id="f_pwd" name="pwd"
							placeholder="Password" value="" required> <span
							class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
						<input type="hidden" id="connectionStatus" name="connectionStatus"
							value="<%=connectionStatus%>">
						<button class="login100-form-btn" type="button"
							onclick="validateUserId()">Validate</button>
						<button class="login100-form-btn" type="submit" value="Submit">
							Login</button>
					</div>

				</form>
				<% } %>

			</div>
		</div>
	</div>


	


</body>
</html>