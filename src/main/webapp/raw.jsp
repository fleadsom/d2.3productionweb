<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper"
	class="deutschebank.core.ApplicationScopeHelper" scope="application" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="dbanalyzer/css/rawJSP.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="dbanalyzer/js/getData.js"></script>

<title>Welcome to Milash App</title>
<link rel="icon" type="image/png" href="https://www.db.com/company/img/favicon.ico" />
</head>

<body background="dbanalyzer/images/background.jpg">
	<nav class="navbar navbar-default" style="background-color: #000000">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Milash</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="home.jsp">Home</a></li>
				<li class="dropdown" style="color: ~ffff"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">Visualization <span
						class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<li><a href="main.jsp">Stock price</a></li>
						<li><a href="combined.jsp">My counterparties</a></li>
						<li><a href="counterp.jsp">My deals</a></li>
					</ul></li>
				<li  class="active"><a href="raw.jsp">Raw Data</a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><span
						class="glyphicon glyphicon-log-out"></span>Logout</a></li>
			</ul>
		</div>
	</nav>
	<div align="right">
		<div class="container">
			<img alt="logo"
				src="https://www.netzwerk-ebd.de/wp-content/uploads/2017/05/logo_square_dbblue-smaller.jpg"
				 width="30" align="right" hspace="15"> <span>Powered
				by</span>
		</div>
	</div>
	<div class="flex-container">
		<div align="center">
			<p style="color: black; font-family: verdana; font-weight: bold;">All
				tables available</p>
		</div>
		<div id="rawDataBtn">
			<button class="button" style="margin: 5px;" id="instrumentBtn">Instrument</button>
			<button class="button" style="margin: 5px;" id="counterpartyBtn">Counterparty</button>
			<button class="button" style="margin: 5px;" id="dealBtn">Deal</button>
		</div>
		<div id="rawDealTableArea"></div>
		<div id="rawInstrumentTableArea"></div>
		<div id="rawCounterpartyTableArea"></div>
	</div>

</body>
</html>
