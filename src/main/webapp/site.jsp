<%-- 
    Document   : index
    Created on : 17-Jun-2016, 16:53:18
    Author     : Selvyn
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper"
	class="deutschebank.core.ApplicationScopeHelper" scope="application" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<title>Welcome to Milash App</title>
<link rel="icon" type="image/png" href="https://www.db.com/company/img/favicon.ico" />

</head>

<body background="dbanalyzer/images/background.jpg">
<nav class="navbar navbar-default" style="background-color: #000000">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Milash</a>
			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="home.jsp">Home</a></li>
				<li class="dropdown" style="color: ~ffff"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">Visualization <span
						class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<li><a href="main.jsp">Stock price</a></li>
						<li><a href="combined.jsp">My counterparties</a></li>
						<li><a href="counterp.jsp">My deals</a></li>
					</ul></li>
				<li><a href="raw.jsp">Raw Data</a></li>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.jsp"><span
						class="glyphicon glyphicon-log-out"></span>Logout</a></li>
			</ul>
		</div>
	</nav>
	<div align="right">
		<div class="container">
			<img alt="logo"
				src="https://www.netzwerk-ebd.de/wp-content/uploads/2017/05/logo_square_dbblue-smaller.jpg"
				 width="30" align="right" hspace="15"> <span>Powered
				by</span>
		</div>
	</div>
	<h1>Sitemap</h1>

	JSP Page
	<ul class="list-unstyled components">
		<li><a href="/dbanalyzer/index.jsp">/dbanalyzer/index.jsp</a></li>
		<li><a href="/dbanalyzer/main.jsp">/dbanalyzer/main.jsp</a></li>
		<li><a href="/dbanalyzer/individual.jsp">/dbanalyzer/individual.jsp</a></li>
		<li><a href="/dbanalyzer/combined.jsp">/dbanalyzer/combined.jsp</a></li>
		<li><a href="/dbanalyzer/raw.jsp">/dbanalyzer/raw.jsp</a></li>
		<li><a href="/dbanalyzer/requirement.jsp">/dbanalyzer/requirement.jsp</a></li>
	</ul>

	Default
	<ul class="list-unstyled components">
		<li><a href="/dbanalyzer/rws/services/sayhello">/dbanalyzer/rws/services/sayhello</a></li>
	</ul>
	Individual
	<ul class="list-unstyled components">
		<li><a href="/dbanalyzer/rws/individual/instrument">/dbanalyzer/rws/individual/instrument</a></li>
		<li><a href="/dbanalyzer/rws/individual/counterparty">/dbanalyzer/rws/individual/counterparty</a></li>
		<li><a href="/dbanalyzer/rws/individual/deal">/dbanalyzer/rws/individual/deal</a></li>
	</ul>
	Requirement
	<ul class="list-unstyled components">
		<li><a href="/dbanalyzer/rws/requirement/reqone">/dbanalyzer/rws/requirement/reqone</a></li>
		<li><a href="/dbanalyzer/rws/requirement/reqtwo">/dbanalyzer/rws/requirement/reqtwo</a></li>
		<li><a href="/dbanalyzer/rws/requirement/reqthree">/dbanalyzer/rws/requirement/reqthree</a></li>
		<li><a href="/dbanalyzer/rws/requirement/reqfour">/dbanalyzer/rws/requirement/reqfour</a></li>
	</ul>



</body>
</html>
